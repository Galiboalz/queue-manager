from twisted.internet import protocol, reactor
from twisted.protocols import basic
import json
import cmd


class Echo(basic.LineReceiver):
    delimiter = b'\n'

    msg = {"response": None}

    def dataReceived(self, data):
        data = json.loads(data)
        self.msg["response"] = data["command"] + ' ' + data["id"]
        data = json.dumps(self.msg)
        self.sendLine(data.encode())


class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        print("Server online.")
        return Echo()


reactor.listenTCP(5678, EchoFactory())
reactor.run()




import cmd


class QueueManager(cmd.Cmd):

    prompt = ''  # arrumando o cmd

    opDisc = {'A': 'available', 'B': 'available'}  # estruturas
    callDisc = {'A': None, 'B': None}
    queueList = []

    # metodos de apoio
    def set_op(self):
        for op in self.opDisc.keys():
            if self.opDisc[op] == 'available':
                return op
        return None

    def get_op(self, cid):
        for op in self.callDisc.keys():
            if self.callDisc[op] == cid:
                return op
        return None

    def set_call(self, op, cid):
        self.callDisc[op] = cid
        self.opDisc[op] = 'ringing'
        print("Call {} ringing for operator {}".format(cid, op))

    def to_queue(self, cid, t):
        if t == 'sd':
            self.queueList.append(cid)
        elif t == 'hp':
            self.queueList.insert(0, cid)
        print("Call {} waiting in queue".format(cid))


    # metodos do gerenciador
    def do_call(self, cid):
        op = self.set_op()
        print("Call {} received".format(cid))
        if op == None:
            self.to_queue(cid, 'sd')
        else:
            self.set_call(op, cid)

    def do_answer(self, op):
        self.opDisc[op] = 'busy'
        print("Call {} answered by operator {}".format(self.callDisc[op], op))

    def do_reject(self, op):
        cid = self.callDisc[op]
        print("Call {} rejected by operator {}".format(cid, op))
        nop = self.set_op()
        self.opDisc[op] = 'available'

        if len(self.queueList) > 0:
            self.set_call(op, self.queueList[0])
            self.queueList.pop(0)

        if nop != None:
            self.set_call(nop, cid)
        elif self.opDisc[op] == 'available':
            self.set_call(op, cid)
        elif self.opDisc[op] != 'available':
            self.to_queue(cid, 'hp')

    def do_hangup(self, cid):
        op = self.get_op(cid)

        if op == None:
            self.queueList.remove(cid)
            print("Call {} missed".format(cid))
        elif self.opDisc[op] == 'ringing':
            print("Call {} missed".format(cid))
            self.opDisc[op] = 'available'
            self.callDisc[op] = None
        elif self.opDisc[op] == 'busy':
            print("Call {} finished and operator {} available".format(cid, op))
            self.opDisc[op] = 'available'
            self.callDisc[op] = None

        if op != None and len(self.queueList) > 0:
            self.set_call(op, self.queueList[0])
            self.queueList.pop(0)



    # sobrescrita de cmd

    def emptyline(self):
        pass

    def do_sum(self, arg):
        for i in self.opDisc.keys():
            print(str(i) + ' ' + str(self.opDisc[i] + ' ' + str(self.callDisc[i])))
        for i in self.queueList:
            print(i)

    def do_close(self, arg):
        return True


QueueManager().cmdloop()

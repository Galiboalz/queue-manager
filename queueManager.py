from twisted.internet import protocol, reactor
from twisted.protocols import basic
import json


class Echo(basic.LineReceiver):
    delimiter = b'\n'

    msg = {"response": None}
    manager = None

    def dataReceived(self, data):
        data = json.loads(data)
        if data["command"] == 'call':
            self.manager.call(data["id"])
        elif data["command"] == 'answer':
            self.manager.answer(data["id"])
        elif data["command"] == 'reject':
            self.manager.reject(data["id"])
        elif data["command"] == 'hangup':
            self.manager.hangup(data["id"])
        elif data["command"] == 'sum':
            self.manager.sum()
        elif data["command"] == 'close':
            self.manager.close()


class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        print("Server online.")
        client = Echo()
        QueueManager(client)
        return client


class QueueManager:

    opDisc = {'A': 'available', 'B': 'available'}  # estruturas
    callDisc = {'A': None, 'B': None}
    checkDisc = {'A': True, 'B': True}
    taskDisc = {'A': None, 'B': None}
    queueList = []

    def __init__(self, client):
        self.client = client
        client.manager = self

    # metodos de apoio
    def set_op(self):
        for op in self.opDisc.keys():
            if self.opDisc[op] == 'available':
                return op
        return None

    def get_op(self, cid):
        for op in self.callDisc.keys():
            if self.callDisc[op] == cid:
                return op
        return None

    def set_call(self, op, cid):
        self.callDisc[op] = cid
        self.opDisc[op] = 'ringing'
        self.client.msg["response"] = "Call {} ringing for operator {}".format(cid, op)
        self.send()
        # print("Call {} ringing for operator {}".format(cid, op))
        if self.checkDisc[op]:
            self.checkDisc[op] = False
            self.taskDisc[op] = reactor.callLater(10, self.ignore, op, cid)


    def to_queue(self, cid, t):
        if t == 'sd':
            self.queueList.append(cid)
        elif t == 'hp':
            self.queueList.insert(0, cid)
        self.client.msg["response"] = "Call {} waiting in queue".format(cid)
        self.send()
        # print("Call {} waiting in queue".format(cid))

    def send(self):
        self.client.sendLine(json.dumps(self.client.msg).encode())
        # print(self.client.msg["response"])


    # metodos do gerenciador
    def call(self, cid):
        op = self.set_op()
        self.client.msg["response"] = "Call {} received".format(cid)
        self.send()
        # print("Call {} received".format(cid))
        if op == None:
            self.to_queue(cid, 'sd')
        else:
            self.set_call(op, cid)

    def answer(self, op):
        self.opDisc[op] = 'busy'
        self.client.msg["response"] = "Call {} answered by operator {}".format(self.callDisc[op], op)
        self.send()
        # print("Call {} answered by operator {}".format(self.callDisc[op], op))

    def reject(self, op):
        cid = self.callDisc[op]
        self.client.msg["response"] = "Call {} rejected by operator {}".format(cid, op)
        self.send()
        # print("Call {} rejected by operator {}".format(cid, op))
        nop = self.set_op()
        self.opDisc[op] = 'available'
        if not self.checkDisc[op]:
            self.taskDisc[op].cancel()
            self.checkDisc[op] = True

        if len(self.queueList) > 0:
            self.set_call(op, self.queueList[0])
            self.queueList.pop(0)

        if nop != None:
            self.set_call(nop, cid)
        elif self.opDisc[op] == 'available':
            self.set_call(op, cid)
        elif self.opDisc[op] != 'available':
            self.to_queue(cid, 'hp')

    def hangup(self, cid):
        op = self.get_op(cid)

        if op == None:
            self.queueList.remove(cid)
            self.client.msg["response"] = "Call {} missed".format(cid)
            self.send()
            # print("Call {} missed".format(cid))
        elif self.opDisc[op] == 'ringing':
            self.client.msg["response"] = "Call {} missed".format(cid)
            self.send()
            # print("Call {} missed".format(cid))
            self.opDisc[op] = 'available'
            self.callDisc[op] = None
        elif self.opDisc[op] == 'busy':
            self.client.msg["response"] = "Call {} finished and operator {} available".format(cid, op)
            self.send()
            # print("Call {} finished and operator {} available".format(cid, op))
            self.opDisc[op] = 'available'
            self.callDisc[op] = None

        if op != None and len(self.queueList) > 0:
            self.set_call(op, self.queueList[0])
            self.queueList.pop(0)


    def ignore(self, op, cid):
        if self.opDisc[op] == 'ringing' and self.callDisc[op] == cid:
            self.client.msg["response"] = "Call {} ignored by operator {}".format(cid, op)
            self.send()
            # print("Call {} ignored by operator {}".format(cid, op))
            nop = self.set_op()
            self.opDisc[op] = 'available'
            self.checkDisc[op] = True


            if len(self.queueList) > 0:
                self.set_call(op, self.queueList[0])
                self.queueList.pop(0)

            if nop != None:
                self.set_call(nop, cid)
            elif self.opDisc[op] == 'available':
                self.set_call(op, cid)
            elif self.opDisc[op] != 'available':
                self.to_queue(cid, 'hp')

    # sobrescrita de cmd
    def sum(self):
        for i in self.opDisc.keys():
            print(str(i) + ' ' + str(self.opDisc[i] + ' ' + str(self.callDisc[i])))
        for i in self.queueList:
            print(i)

    def close(self):
        return self.client.reactor.stop()


reactor.listenTCP(5678, EchoFactory())
reactor.run()

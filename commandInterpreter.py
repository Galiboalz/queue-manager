import cmd
import json
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.protocols import basic


class EchoClient(basic.LineReceiver):

    delimiter = b'\n'

    def lineReceived(self, line):
        line = json.loads(line)
        print(line['response'])


    #def dataReceived(self, data):
    #    data = json.loads(data)
    #    print(data)


class EchoFactory(protocol.ClientFactory):
    def buildProtocol(self, addr):
        client = EchoClient()
        reactor.callInThread(CommandInterpreter(client).cmdloop)
        return client

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed.")
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("Connection lost.")
        reactor.stop()


class CommandInterpreter(cmd.Cmd):
    prompt = ''

    msg = {"command": None, "id": None}

    def __init__(self, client):
        cmd.Cmd.__init__(self)
        self.client = client

    def do_call(self, cid):
        self.msg["command"] = "call"
        self.msg["id"] = cid
        self.send()

    def do_answer(self, op):
        self.msg["command"] = "answer"
        self.msg["id"] = op
        self.send()

    def do_reject(self, op):
        self.msg["command"] = "reject"
        self.msg["id"] = op
        self.send()

    def do_hangup(self, cid):
        self.msg["command"] = "hangup"
        self.msg["id"] = cid
        self.send()

    def do_close(self, arg):
        self.client.transport.loseConnection()
        return True

    def do_sum(self, arg):
        self.msg["command"] = "sum"
        self.msg["id"] = None
        self.send()

    def send(self):
        data = json.dumps(self.msg)
        self.client.sendLine(data.encode())


reactor.connectTCP("localhost", 5678, EchoFactory())
reactor.run()

